const http = require('http');
const fs = require('fs');
var url = require('url');

http.createServer((req, res) => {

    if (req.url == '/index.html' || req.url == '/') {

        fs.readFile('index.html', (err, data) => {

            if (err) {
                console.log(err);
            } else {
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.write(data);
                res.end();
                console.log('Web Server Iniciado ...');
            }
        });

    }

    console.log('Server online ..')

}).listen(8000);