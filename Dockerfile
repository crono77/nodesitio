FROM node:10-alpine

WORKDIR /home/node/app

COPY . .

CMD [ "node", "app" ]